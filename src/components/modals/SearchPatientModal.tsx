import { Dialog, Transition } from "@headlessui/react";
import { useSnackbar } from "notistack";
import { type FormEvent, Fragment, forwardRef } from "react";
import invariant from "tiny-invariant";
import Button from "../Button";
import { useSearchParams } from "next/navigation";
import { useRouter } from "next/router";

type SearchPatientModalProps = {
  onClose: () => void;
  open: boolean;
};

const SearchPatient = forwardRef<HTMLDivElement, SearchPatientModalProps>(
  ({ onClose }, ref) => {
    const router = useRouter();
    const params = useSearchParams();
    const { enqueueSnackbar } = useSnackbar();

    // TODO: why did i need this?
    const page = params.get("page") ?? "1";

    const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
      try {
        e.preventDefault();
        const data = new FormData(e.currentTarget as HTMLFormElement);

        const name = data.get("name")?.toString();
        const surname = data.get("surname")?.toString();
        const documentValue = data.get("documentValue")?.toString();

        invariant(
          !!name || !!surname || !!documentValue,
          "Por favor, rellene alguno de los campos",
        );

        const queryParams = new URLSearchParams();

        name && queryParams.set("name", name);
        surname && queryParams.set("surname", surname);
        documentValue && queryParams.set("documentValue", documentValue);

        const queryParamsString = queryParams.toString();

        void router.push(
          `/app/patients${queryParamsString ? `?${queryParamsString}` : ""}`,
        );
      } catch (error) {
        const typedError = error as Error;
        const invariantMsg = typedError?.message.includes("Invariant")
          ? typedError.message.split(":")[1]
          : null;

        enqueueSnackbar({
          variant: "error",
          message:
            invariantMsg ??
            "Por favor, consulte con el administrador del sistema",
        });
      }
    };

    return (
      <Dialog.Panel
        ref={ref}
        className="w-full max-w-xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all"
      >
        <Dialog.Title
          as="h3"
          className="text-lg font-medium leading-6 text-gray-900"
        >
          Buscar paciente
        </Dialog.Title>
        <div className="mt-2">
          <form className="space-y-4" onSubmit={handleSubmit}>
            <label>
              <span className="text-sm font-medium text-gray-600">
                Nombre del paciente
              </span>
              <input
                className="focus:ring-primary-color w-full rounded-lg border px-4 py-2 focus:ring"
                defaultValue={params.get("name") ?? ""}
                type="text"
                name="name"
              />
            </label>

            <label>
              <span className="text-sm font-medium text-gray-600">
                Apellido del paciente
              </span>
              <input
                defaultValue={params.get("surname") ?? ""}
                className="focus:ring-primary-color w-full rounded-lg border px-4 py-2 focus:ring"
                type="text"
                name="surname"
              />
            </label>

            <label>
              <span className="text-sm font-medium text-gray-600">
                Nro de documento
              </span>
              <input
                defaultValue={params.get("documentValue") ?? ""}
                className="focus:ring-primary-color w-full rounded-lg border px-4 py-2 focus:ring"
                type="text"
                name="documentValue"
              />
            </label>

            <Button type="submit" className="ml-auto">
              Buscar paciente
            </Button>
          </form>
        </div>
      </Dialog.Panel>
    );
  },
);

export default function SearchPatientModal(props: SearchPatientModalProps) {
  return (
    <Transition appear show={props.open} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={props.onClose}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <SearchPatient {...props} />
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
}
