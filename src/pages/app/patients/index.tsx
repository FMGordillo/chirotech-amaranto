import Layout from "~/components/Layout";
import { api } from "~/utils/api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import CreatePatientModal from "~/components/modals/CreatePatientModal";
import AppLayout from "~/components/AppLayout";
import { useSearchParams } from "next/navigation";
import Pagination from "~/components/Pagination";
import LoadingPatient from "~/components/LoadingPatient";
import invariant from "tiny-invariant";
import Button from "~/components/Button";

const Patients = () => {
  const router = useRouter();
  const params = useSearchParams();
  const [newPatientModal, setNewPatientModal] = useState(false);
  const [filterParams, setFilterParams] = useState<
    { key: string; value: string }[]
  >([]);

  const page = params.get("page") ?? "1";
  const patientId = params.get("patientId");

  const name = params.get("name");
  const surname = params.get("surname");
  const documentValue = params.get("documentValue");

  const hasFilter = !!name || !!surname || !!documentValue;

  const { refetch, data, isLoading } = api.patients.getPatients.useQuery({
    documentValue,
    name,
    page,
    surname,
  });

  useEffect(() => {
    // TODO: this effect runs twice in development, not to worry i think
    if (filterParams.length === 0) {
      for (const iterator of params.entries()) {
        setFilterParams((prev) => [
          ...prev,
          { key: iterator[0], value: iterator[1] },
        ]);
      }
    }
  }, [filterParams.length, params]);

  const handlePatientClick = (patientId: string | undefined) => {
    invariant(!!patientId, "patientId must be provided");
    void router.push(`/app/patients/${patientId}`);
  };

  const handleFilterClick = () => {
    void router.push("/app/patients");
  };

  const handleFilterChange = (key: string) => {
    const habaer = new URLSearchParams(params);
    habaer.delete(key);
    setFilterParams((prev) => prev.filter((p) => p.key !== key));
    void router.push(`/app/patients?${habaer.toString()}`);

    // void router.push(`/app/patients?${key}=${value}`);
  };

  return (
    <Layout title="Pacientes - Amaranto">
      <AppLayout>
        <div className="relative z-10 mb-4 flex items-center justify-between">
          <h2 className="mb-4 text-2xl font-semibold">Lista de pacientes</h2>
          <div className="flex space-x-4">
            {hasFilter && (
              <Button
                type="button"
                variant="outline"
                onClick={handleFilterClick}
              >
                Limpiar filtro
              </Button>
            )}
            <Button type="button" onClick={() => setNewPatientModal(true)}>
              Crear paciente
            </Button>
          </div>
        </div>

        {hasFilter && (
          <div className="mb-4 flex space-x-4">
            {filterParams.map((filter) => (
              <button
                className="rounded bg-gray-200 px-2 py-1 text-sm after:ml-2 after:text-transparent after:content-['X'] hover:after:text-black"
                key={filter.key}
                onClick={() => handleFilterChange(filter.key)}
                type="button"
              >
                {filter.key} = {filter.value}
              </button>
            ))}
          </div>
        )}

        {(data?.patients && data.patients.length > 0) ?? isLoading ? (
          <>
            <table className="mb-4 w-full divide-y divide-gray-200">
              <thead>
                <tr>
                  <th className="px-6 py-3 text-left text-xs font-medium uppercase tracking-wider text-gray-500">
                    Nombre y apellido
                  </th>
                  <th className="px-6 py-3 text-left text-xs font-medium uppercase tracking-wider text-gray-500">
                    Documento
                  </th>
                </tr>
              </thead>

              <tbody className="divide-y divide-gray-200">
                {isLoading && <LoadingPatient />}
                {!isLoading &&
                  data &&
                  data.patients.length > 0 &&
                  data.patients.map((patient) => (
                    <tr
                      key={patient?.id}
                      className={`hover:cursor-pointer hover:bg-fuchsia-50 ${
                        patient?.id === patientId
                          ? "bg-fuchsia-200"
                          : "bg-transparent"
                      }`}
                      onClick={() => handlePatientClick(patient?.id)}
                      onKeyDown={() => handlePatientClick(patient?.id)}
                    >
                      <td className="px-6 py-3">
                        <span className="select-none">
                          {patient?.name} {patient?.surname}
                        </span>
                      </td>
                      <td className="px-6 py-3">
                        <span className="select-none">
                          {patient?.documentType?.toUpperCase()}{" "}
                          {patient?.documentValue}
                        </span>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>

            {data && (
              <Pagination
                visiblePages={data?.visiblePages}
                hasPreviousPage={data?.hasPreviousPage}
                hasNextPage={data?.hasNextPage}
                pages={data?.pages}
              />
            )}
          </>
        ) : (
          <p className="text-center text-lg">No se encuentran pacientes</p>
        )}

        <CreatePatientModal
          open={newPatientModal}
          onClose={() => setNewPatientModal(false)}
          onSubmit={() => void refetch()}
        />
      </AppLayout>
    </Layout>
  );
};

export default Patients;
