import type { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import {
  useState,
  type FunctionComponent,
  type PropsWithChildren,
} from "react";
import AppLayout from "~/components/AppLayout";
import Layout from "~/components/Layout";
import CreatePatientModal, {
  type Patient,
} from "~/components/modals/CreatePatientModal";
import CreateRecordModal, {
  type ClinicalRecord,
} from "~/components/modals/CreateRecordModal";
import SearchPatientModal from "~/components/modals/SearchPatientModal";

const Box: FunctionComponent<PropsWithChildren<{ action: () => void }>> = ({
  action,
  children,
}) => {
  return (
    <button
      type="button"
      className="h-16 border bg-fuchsia-100 p-4 shadow transition-all hover:cursor-pointer hover:bg-fuchsia-200 hover:shadow-lg"
      onClick={action}
    >
      <span className="block text-center">{children}</span>
    </button>
  );
};

const AppPage: NextPage = () => {
  const { data } = useSession();
  const router = useRouter();
  const [showPatientModal, setShowPatientModal] = useState(false);
  const [showSearchPatientModal, setShowSearchPatientModal] = useState(false);
  const [showRecordModal, setShowRecordModal] = useState(false);

  const handlePatientSubmit = (data: Patient[]) => {
    setShowPatientModal(false);
    void router.push(`/app/patients/${data[0]?.id}`);
  };

  const handleRecordSubmit = (data: ClinicalRecord[]) => {
    setShowRecordModal(false);
    void router.push(`/app/patients/${data[0]?.patientId}`);
  };

  return (
    <Layout>
      <AppLayout>
        <h1 className="mb-4 text-2xl">Hola, {data?.user.name}</h1>
        <div className="grid grid-flow-col gap-4">
          <Box action={() => setShowSearchPatientModal(true)}>
            Buscar paciente
          </Box>
          <Box action={() => setShowPatientModal(true)}>
            Crear nuevo paciente
          </Box>
          <Box action={() => setShowRecordModal(true)}>
            Registrar nueva visita
          </Box>
        </div>

        <SearchPatientModal
          open={showSearchPatientModal}
          onClose={() => setShowSearchPatientModal(false)}
        />

        <CreatePatientModal
          open={showPatientModal}
          onSubmit={handlePatientSubmit}
          onClose={() => setShowPatientModal(false)}
        />

        <CreateRecordModal
          open={showRecordModal}
          onSubmit={handleRecordSubmit}
          onClose={() => setShowRecordModal(false)}
        />
      </AppLayout>
    </Layout>
  );
};

export default AppPage;
